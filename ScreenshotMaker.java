package p1;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ScreenshotMaker {

    public void takeScreenshot(WebDriver driver, String result) {
        String pathName = "target/screenshots/" + new Date().getTime() + "_" + result + ".png";
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File targetFile = new File(pathName);
        attachScreenshot(driver);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
        }
    }

    private void attachScreenshot(WebDriver driver) {
        saveScreenshotOfError(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
    }

    @Attachment(value = "step [ {0} ] PASS", type = "text/plain")
    private static String allurePassAttachmentText(String someAttachmentText) {
        return someAttachmentText;
    }

    @Attachment(value = "Screenshot of error", type = "image/png")
    private static byte[] saveScreenshotOfError(byte[] screenshot) {
        return screenshot;
    }
}
