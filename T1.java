package p1;

import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Epics({@Epic("Epic#1"), @Epic("Epic#2"), @Epic("Epic#3")})
public class T1 {

    @Test(testName = "Test_name")
    @Epics({@Epic("Epic#1"), @Epic("Epic#2"), @Epic("Epic#3")})
    @Feature("Feature1")
    @Story("story1")
    @Step("step n1")
    @Description("Descriptions 2")
    public void test1() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://translate.google.com.ua/?hl=ru&tab=wT&authuser=0#ru/en/%D0%97%D0%B0%D0%B2%D0%B5%D1%80%D1%88%D0%B5%D0%BD%D0%B0%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%20%D1%81%20%D0%BD%D0%B0%D0%B1%D0%BE%D1%80%D0%BE%D0%BC%20%D1%82%D0%B5%D1%81%D1%82%D0%BE%D0%B2");

        assertTrue(true);
        new ScreenshotMaker().takeScreenshot(driver, "this");
        driver.close();
    }
}
